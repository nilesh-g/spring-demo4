package com.sunbeaminfo.sh.services;

import java.util.List;

import com.sunbeaminfo.sh.entities.Book;

public interface BookService {

	Book getBook(int id);

	void addBook(Book b);

	void updateBook(Book b);

	void deleteBook(int id);

	List<Book> getBooks();

	List<Book> getSubjectBooks(String subject);

	List<String> getSubjects();

}