package com.sunbeaminfo.sh.main;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sunbeaminfo.sh.entities.Book;
import com.sunbeaminfo.sh.services.BookService;

public class Sp04Main {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx;
		ctx = new ClassPathXmlApplicationContext("/application.xml");
	
		BookService bookService = ctx.getBean(BookService.class);
		List<Book> list = bookService.getBooks();
		for (Book b : list)
			System.out.println(b);
		
		ctx.close();
	}
}
